package com.nelson.testapi.api.interfaces.i_api;

import com.nelson.testapi.api.models.m_api.MDate;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface IDate {

    MDate IaddDate(Connection cnx, MDate mDate) throws Exception;

    List<MDate> IgetAllDate(Connection cnx) throws SQLException;

}
