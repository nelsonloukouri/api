package com.nelson.testapi.api.interfaces.i_api;

import com.nelson.testapi.api.models.m_api.MFichier;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface IFichier {

    MFichier IaddFichier(Connection cnx, MFichier mFichier) throws Exception;

    List<MFichier> IgetAllFichier(Connection cnx) throws SQLException;

    MFichier IupdateFichier(Connection cnx, MFichier mFichier) throws Exception;

    MFichier IdeleteFichier(Connection cnx, MFichier mFichier) throws Exception;

}
