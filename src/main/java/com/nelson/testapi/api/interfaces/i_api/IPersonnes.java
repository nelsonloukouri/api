package com.nelson.testapi.api.interfaces.i_api;

import com.nelson.testapi.api.models.m_api.MPersonnes;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public interface IPersonnes {

    MPersonnes IaddPersonnes(Connection cnx, MPersonnes mPersonnes) throws Exception;
    List<MPersonnes> IgetAllPersonnes(Connection cnx) throws SQLException;
    MPersonnes IupdatePersonnes(Connection cnx, MPersonnes mPersonnes) throws Exception;
    MPersonnes IdeletePersonnes(Connection cnx, MPersonnes mPersonnes) throws Exception;

}
