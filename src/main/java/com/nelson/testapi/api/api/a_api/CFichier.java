package com.nelson.testapi.api.api.a_api;

import com.nelson.testapi.api.interfaces.i_api.IFichier;
import com.nelson.testapi.api.models.m_api.MFichier;
import com.nelson.testapi.api.services.Connexion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping(value = "/api")
public class CFichier {

    @Autowired
    private final IFichier iFichier;

    @Autowired
    private final Connexion cnx;

    public CFichier(IFichier iFichier, Connexion cnx) {
        this.iFichier = iFichier;
        this.cnx = cnx;
    }

    @RequestMapping(value = "/addFichier", method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
    private Map<String, Object> addFichier(@RequestBody MFichier mFichier) throws SQLException {

        Map<String, String> message = new HashMap<>();
        Map<String, Object> response = new HashMap<>();
        MFichier fichier = new MFichier();
        try {
            fichier = iFichier.IaddFichier(cnx.apiCnx(), mFichier);
            if (fichier == null) {
                message.put("code", "204");
                message.put("message", "Aucune donnée inséré");
                response.put("reponse", message);
                response.put("data", fichier);
            } else {
                message.put("code", "200");
                message.put("message", "requête executée avec succes");
                response.put("reponse", message);
                response.put("data", fichier);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error SQLException");
            response.put("response", message);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error ClassNotFoundException");
            response.put("response", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    @RequestMapping(value = "/getAllFichier", method = RequestMethod.GET, produces = {"application/json"})
    private Map<String, Object> getAllFichier() throws SQLException {

        List<MFichier> fichierList = new ArrayList<>();
        Map<String, String> message = new HashMap<>();
        Map<String, Object> response = new HashMap<>();
        try {
            fichierList = iFichier.IgetAllFichier(cnx.apiCnx());
            if (fichierList.isEmpty()) {
                message.put("code", "204");
                message.put("message", "Aucune donnée trouvée");
                response.put("reponse", message);
                response.put("data", fichierList);
            } else {
                message.put("code", "200");
                message.put("message", "requête executée avec succes");
                response.put("reponse", message);
                response.put("data", fichierList);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error SQLException");
            response.put("response", message);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error ClassNotFoundException");
            response.put("response", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    @RequestMapping(value = "/updateFichier", method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
    private Map<String, Object> updateFichier(@RequestBody MFichier mFichier) throws SQLException {

        MFichier fichier = new MFichier();
        Map<String, String> message = new HashMap<>();
        Map<String, Object> response = new HashMap<>();
        try {
            fichier = iFichier.IupdateFichier(cnx.apiCnx(), mFichier);
            if (fichier == null) {
                message.put("code", "204");
                message.put("message", "Aucune donnée inséré");
                response.put("reponse", message);
                response.put("data", fichier);
            } else {
                message.put("code", "200");
                message.put("message", "requête executée avec succes");
                response.put("reponse", message);
                response.put("data", fichier);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error SQLException");
            response.put("response", message);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error ClassNotFoundException");
            response.put("response", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    @RequestMapping(value = "/deleteFichier", method = RequestMethod.POST, consumes = {"application/json"}, produces = {"application/json"})
    private Map<String, Object> deleteFichier(@RequestBody MFichier mFichier) throws SQLException {

        MFichier fichier = new MFichier();
        Map<String, String> message = new HashMap<>();
        Map<String, Object> response = new HashMap<>();
        try {
            fichier = iFichier.IdeleteFichier(cnx.apiCnx(), mFichier);
            if (fichier == null) {
                message.put("code", "204");
                message.put("message", "Aucune donnée inséré");
                response.put("reponse", message);
                response.put("data", fichier);
            } else {
                message.put("code", "200");
                message.put("message", "requête executée avec succes");
                response.put("reponse", message);
                response.put("data", fichier);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error SQLException");
            response.put("response", message);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error ClassNotFoundException");
            response.put("response", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

}
