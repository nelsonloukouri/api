package com.nelson.testapi.api.api.a_api;

import com.nelson.testapi.api.interfaces.i_api.IDate;
import com.nelson.testapi.api.models.m_api.MDate;
import com.nelson.testapi.api.services.Connexion;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin("*")
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/testDate")
public class CDate {

    @Autowired
    private final IDate iDate;

    @Autowired
    private final Connexion cnx;

    @PostMapping(value = "/addDate")
    private Map<String, Object> addDate(@RequestBody MDate mDate) {

        Map<String, String> message = new HashMap<>();
        Map<String, Object> response = new HashMap<>();
        try {
            MDate date = new MDate();
            date = iDate.IaddDate(cnx.apiCnx(), mDate);
            if (date == null) {
                message.put("code", "204");
                message.put("message", "Aucune donnée inséré");
                response.put("reponse", message);
            } else {
                message.put("code", "200");
                message.put("message", "requête executée avec succes");
                response.put("reponse", message);
                response.put("data", date);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error SQLException");
            response.put("response", message);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error ClassNotFoundException");
            response.put("response", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    @GetMapping(value = "/getAllDate")
    private Map<String, Object> getAllDate() {

        Map<String, String> message = new HashMap<>();
        Map<String, Object> response = new HashMap<>();
        try {
            List<MDate> dateList = new ArrayList<>();
            dateList = iDate.IgetAllDate(cnx.apiCnx());
            if (dateList == null) {
                message.put("code", "204");
                message.put("message", "Aucune donnée inséré");
                response.put("reponse", message);
            } else {
                message.put("code", "200");
                message.put("message", "requête executée avec succes");
                response.put("reponse", message);
                response.put("data", dateList);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error SQLException");
            response.put("response", message);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            message.put("code", "209");
            message.put("message", "Internal server Error ClassNotFoundException");
            response.put("response", message);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

}
