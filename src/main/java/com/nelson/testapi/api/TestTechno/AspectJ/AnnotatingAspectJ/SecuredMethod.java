package com.nelson.testapi.api.TestTechno.AspectJ.AnnotatingAspectJ;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SecuredMethod {

    private static final Logger logger = LoggerFactory.getLogger(SecuredMethod.class);

    @Secured(isLocked = true)
    public void lockedMethod() {
        logger.info("lockedMethod");
    }

    @Secured(isLocked = false)
    public void unLockedMethode() {
        logger.info("unlockedMethod");
    }

    public static void main(String[] args) throws Exception {
        SecuredMethod sv = new SecuredMethod();
        sv.lockedMethod();
    }

}
