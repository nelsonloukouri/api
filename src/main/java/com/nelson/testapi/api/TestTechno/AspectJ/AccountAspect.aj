package com.nelson.testapi.api.TestTechno.AspectJ;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public aspect AccountAspect {

    final int MIN_BALANCE = 10;

    private static final Logger logger = LoggerFactory.getLogger(AccountAspect.class);

    pointcut callWithDraw(int amount, Account acc) :
            call(boolean Account.withdraw(int)) && args(amount) && target(acc);

    before(int amount, Account acc) : callWithDraw(amount, acc) {
        logger.info(" Balance before withdrawal: {}", acc.balance);
        logger.info(" Withdraw ammout: {}", amount);
    }

    boolean around(int amount, Account acc) :
            callWithDraw(int amount, Account acc) {
        if (acc.balance < amount) {
            logger.info("Withdrawal Rejected!");
            return false;
        }
        return proceed(amount, acc);
    }

    after(int amount, Account balance) : callWithDraw(amount, balance) {
        logger.info("Balance after withdrawal : {}", balance.balance);
    }




}
