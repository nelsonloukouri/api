package com.nelson.testapi.api;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;
import java.time.LocalTime;

@RestController
public class testCron {

//    @Scheduled(cron = "*/5 * * * * ?")
    public void scheduleTaskUsingCronExpression() {
        LocalTime testLocalTime = LocalTime.now();
        String xlt = "ça marche !";
        System.out.println(
                "tâche programmée avec les expressions cron - " + xlt + "-> " + testLocalTime + "\n");

    }

}
