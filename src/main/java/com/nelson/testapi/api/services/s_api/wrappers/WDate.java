package com.nelson.testapi.api.services.s_api.wrappers;

import com.nelson.testapi.api.models.m_api.MDate;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

@Service
public class WDate implements Function<ResultSet, MDate> {
    @Override
    public MDate apply(ResultSet resultSet) {
        MDate date = new MDate();
        try {
            date.setIdDate(resultSet.getInt("idDate"));
            date.setDate(resultSet.getDate("date_"));
            date.setCreerle(resultSet.getTime("creerle"));
            date.setCreatedBy(resultSet.getString("createdBy"));
            date.setModifiedBy(resultSet.getString("modifiedBy"));
//            date.setIsDeleted(resultSet.getBoolean("isDeleted"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return date;
    }

}
