package com.nelson.testapi.api.services;

import com.nelson.testapi.api.Utils.FileStorageProperties;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;

@Service
public class FileStorageService {

    private final Path fileStorageLocation;

    public FileStorageService(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Map<String, String> saveFile(MultipartFile file, String typeFichier, int id) {

        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        Map<String, String> response = new HashMap<>();
        if (fileName.contains("..")) {
            response.put("code", "500");
            response.put("message", "file contains incorrect caractères");
            return response;
        } else if (!FilenameUtils.getExtension(fileName).equals("pdf")) {
            response.put("code", "500");
            response.put("message", "incorrect file!!!, please upload pdf file");
            return response;
        }

        // New file contient le nom du fichier
        String newFile = typeFichier + "" + id + "." + FilenameUtils.getExtension(fileName);
        try {
            Path targetLocation = this.fileStorageLocation.resolve(newFile);
            System.out.println("file : " + file.getOriginalFilename());
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            response.put("code", "200");
            response.put("message", newFile);
        } catch (IOException e) {
            e.printStackTrace();
            response.put("code", "500");
            response.put("message", e.getMessage());
        }
        return response;
    }
}
