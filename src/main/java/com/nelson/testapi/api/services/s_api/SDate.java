package com.nelson.testapi.api.services.s_api;

import com.nelson.testapi.api.interfaces.i_api.IDate;
import com.nelson.testapi.api.models.m_api.MDate;
import com.nelson.testapi.api.services.s_api.wrappers.WDate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Service("IDate")
@RequiredArgsConstructor
public class SDate implements IDate {

    private final WDate wDate;

    @Override
    public MDate IaddDate(Connection cnx, MDate mDate) throws Exception {
        CallableStatement callableStatement;
        ResultSet resultSet;
        System.out.println("debut IaddDate");
        MDate date = new MDate();
        try {
            callableStatement = cnx.prepareCall("CALL api.ps_addDate(?)");
            callableStatement.setDate("P_date", mDate.getDate());
            callableStatement.executeUpdate();
            resultSet = callableStatement.getResultSet();
            while (resultSet.next()) {
                date = wDate.apply(resultSet);            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            cnx.close();
        }
        System.out.println("fin IaddDate");
        return date;
    }

    @Override
    public List<MDate> IgetAllDate(Connection cnx) throws SQLException {
        CallableStatement callableStatement;
        ResultSet resultSet;
        System.out.println("debut IgetAllDate");
        List<MDate> dateList = new ArrayList<>();
        try {
            callableStatement = cnx.prepareCall("CALL api.ps_getAllDate()");
            callableStatement.executeUpdate();
            resultSet = callableStatement.getResultSet();
            while (resultSet.next()) {
                MDate date = new MDate();
                date = wDate.apply(resultSet);
                dateList.add(date);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            cnx.close();
        }
        System.out.println("fin IgetAllDate");
        return dateList;
    }

}
