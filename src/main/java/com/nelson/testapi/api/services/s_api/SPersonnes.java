package com.nelson.testapi.api.services.s_api;

import com.nelson.testapi.api.interfaces.i_api.IPersonnes;
import com.nelson.testapi.api.models.m_api.MPersonnes;
import com.nelson.testapi.api.services.s_api.wrappers.WPersonnes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service("IPersonnes")
public class SPersonnes implements IPersonnes {

    @Autowired
    private WPersonnes wPersonnes;

    @Override
    public MPersonnes IaddPersonnes(Connection cnx, MPersonnes mPersonnes) throws Exception {
        CallableStatement callableStatement;
        ResultSet resultSet;
        System.out.println("debut IaddPersonne");
        MPersonnes personne = new MPersonnes();
        try {
            callableStatement = cnx.prepareCall("CALL api.ps_addPersonnes(?,?,?,?,?,?,?,?)");
            callableStatement.setString("P_nom", mPersonnes.getGenre());
            callableStatement.setString("P_prenoms", mPersonnes.getPrenoms());
            callableStatement.setInt("P_age", mPersonnes.getAge());
            callableStatement.setString("P_email", mPersonnes.getEmail());
            callableStatement.setString("P_motDePasse", mPersonnes.getMotDePasse());
            callableStatement.setString("P_ville", mPersonnes.getVille());
            callableStatement.setString("P_adresse", mPersonnes.getAdresse());
            callableStatement.setString("P_genre", mPersonnes.getGenre());
            callableStatement.executeUpdate();
            resultSet = callableStatement.getResultSet();
//            if (resultSet.)
            while (resultSet.next()) {
                personne = wPersonnes.apply(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            cnx.close();
        }
        System.out.println("fin IaddPersonne");
        return personne;
    }
    @Override
    public List<MPersonnes> IgetAllPersonnes(Connection cnx) throws SQLException {
        CallableStatement callableStatement;
        ResultSet resultSet;
        System.out.println("debut IgetAllPersonnes");
        List<MPersonnes> personnesList = new ArrayList<>();
        try {
            callableStatement = cnx.prepareCall("CALL api.ps_getAllPersonnes()");
            callableStatement.executeUpdate();
            resultSet = callableStatement.getResultSet();
            while (resultSet.next()) {
                MPersonnes personne = new MPersonnes();
                personne = wPersonnes.apply(resultSet);
                personnesList.add(personne);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cnx.close();
        }
        System.out.println("fin IgetAllPersonnes");
        return personnesList;
    }
    @Override
    public MPersonnes IupdatePersonnes(Connection cnx, MPersonnes mPersonnes) throws Exception {
        CallableStatement callableStatement;
        ResultSet resultSet;
        System.out.println("debut IupdatePersonne");
        MPersonnes personne = new MPersonnes();
        try {
            callableStatement = cnx.prepareCall("CALL api.ps_updatePersonnes(?,?,?,?,?,?,?,?,?)");
            callableStatement.setInt("P_idPersonne", mPersonnes.getIdPersonne());
            callableStatement.setString("P_nom", mPersonnes.getGenre());
            callableStatement.setString("P_prenoms", mPersonnes.getPrenoms());
            callableStatement.setInt("P_age", mPersonnes.getAge());
            callableStatement.setString("P_email", mPersonnes.getEmail());
            callableStatement.setString("P_motDePasse", mPersonnes.getMotDePasse());
            callableStatement.setString("P_ville", mPersonnes.getVille());
            callableStatement.setString("P_adresse", mPersonnes.getAdresse());
            callableStatement.setString("P_genre", mPersonnes.getGenre());
            callableStatement.executeUpdate();
            resultSet = callableStatement.getResultSet();
            while (resultSet.next()) {
                personne = wPersonnes.apply(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            cnx.close();
        }
        System.out.println("fin IupdatePersonne");
        return personne;
    }
    @Override
    public MPersonnes IdeletePersonnes(Connection cnx, MPersonnes mPersonnes) throws Exception {
        CallableStatement callableStatement;
        ResultSet resultSet;
        System.out.println("debut IdeletePersonne");
        MPersonnes personne = new MPersonnes();
        try {
            callableStatement = cnx.prepareCall("CALL api.ps_deletePersonnes(?)");
            callableStatement.setInt("P_idPersonne", mPersonnes.getIdPersonne());
            callableStatement.executeUpdate();
            resultSet = callableStatement.getResultSet();
            while (resultSet.next()) {
                personne = wPersonnes.apply(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            cnx.close();
        }
        System.out.println("fin IdeletePersonne");
        return personne;
    }

}
