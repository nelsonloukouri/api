package com.nelson.testapi.api.services.s_api;

import com.nelson.testapi.api.interfaces.i_api.IFichier;
import com.nelson.testapi.api.models.m_api.MFichier;
import com.nelson.testapi.api.services.s_api.wrappers.WFichier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service("IFichier")
public class SFichier implements IFichier {

    @Autowired
    private WFichier wFichier;

    @Override
    public MFichier IaddFichier(Connection cnx, MFichier mFichier) throws Exception {
        CallableStatement callableStatement;
        ResultSet resultSet;
        System.out.println("debut IaddFichier");
        MFichier fichier = new MFichier();
        try {
            callableStatement = cnx.prepareCall("CALL api.ps_addfichier(?,?,?)");
            callableStatement.setInt("P_idEntite", mFichier.getIdEntite());
            callableStatement.setString("P_nomFichier", mFichier.getNomFichier());
            callableStatement.setString("P_typeFichier", mFichier.getTypeFichier());
            callableStatement.executeUpdate();
            resultSet = callableStatement.getResultSet();
            while (resultSet.next()) {
                fichier = wFichier.apply(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            cnx.close();
        }
        System.out.println("fin IaddFichier");
        return fichier;
    }

    @Override
    public List<MFichier> IgetAllFichier(Connection cnx) throws SQLException {
        CallableStatement callableStatement;
        ResultSet resultSet;
        System.out.println("debut IgetAllFichier");
        List<MFichier> fichierList = new ArrayList<>();
        try {
            callableStatement = cnx.prepareCall("CALL api.ps_getAllFichier()");
            callableStatement.executeUpdate();
            resultSet = callableStatement.getResultSet();
            while (resultSet.next()) {
                MFichier fichier = new MFichier();
                fichier = wFichier.apply(resultSet);
                fichierList.add(fichier);
            }
            resultSet.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            cnx.close();
        }
        System.out.println("fin IgetAllFichier");
        return fichierList;
    }

    @Override
    public MFichier IupdateFichier(Connection cnx, MFichier mFichier) throws Exception {
        CallableStatement callableStatement;
        ResultSet resultSet;
        System.out.println("debut IupdateFichier");
        MFichier fichier = new MFichier();
        try {
            callableStatement = cnx.prepareCall("CALL api.ps_updateFichier(?,?,?,?)");
            callableStatement.setInt("P_idFichier", mFichier.getIdFichier());
            callableStatement.setInt("P_idEntite", mFichier.getIdEntite());
            callableStatement.setString("P_nomFichier", mFichier.getNomFichier());
            callableStatement.setString("P_typeFichier", mFichier.getTypeFichier());
            callableStatement.executeUpdate();
            resultSet = callableStatement.getResultSet();
            while (resultSet.next()) {
                fichier = wFichier.apply(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            cnx.close();
        }
        System.out.println("fin IupdateFichier");
        return fichier;
    }

    @Override
    public MFichier IdeleteFichier(Connection cnx, MFichier mFichier) throws Exception {
        CallableStatement callableStatement;
        ResultSet resultSet;
        System.out.println("debut IdeleteFichier");
        MFichier fichier = new MFichier();
        try {
            callableStatement = cnx.prepareCall("CALL api.ps_deleteFichier(?)");
            callableStatement.setInt("P_idFichier", mFichier.getIdFichier());
            callableStatement.executeUpdate();
            resultSet = callableStatement.getResultSet();
            while (resultSet.next()) {
                fichier = wFichier.apply(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            cnx.close();
        }
        System.out.println("fin IdeleteFichier");
        return fichier;
    }

}
