package com.nelson.testapi.api.services.s_api.wrappers;

import com.nelson.testapi.api.models.m_api.MPersonnes;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

@Service
public class WPersonnes implements Function<ResultSet, MPersonnes> {

    @Override
    public MPersonnes apply(ResultSet resultSet) {
        MPersonnes personne = new MPersonnes();
        try {
            personne.setIdPersonne(resultSet.getInt("idPersonne"));
            personne.setNom(resultSet.getString("nom"));
            personne.setPrenoms(resultSet.getString("prenoms"));
            personne.setAge(resultSet.getInt("age"));
            personne.setEmail(resultSet.getString("email"));
            personne.setCreatedBy(resultSet.getString("createdBy"));
            personne.setModifiedBy(resultSet.getString("modifiedBy"));
            personne.setIsDeleted(resultSet.getBoolean("isDeleted"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return personne;
    }
}
