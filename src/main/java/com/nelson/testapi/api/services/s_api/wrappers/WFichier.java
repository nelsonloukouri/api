package com.nelson.testapi.api.services.s_api.wrappers;

import com.nelson.testapi.api.Utils.DateConverter;
import com.nelson.testapi.api.models.m_api.MFichier;
import org.springframework.stereotype.Service;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.function.Function;

@Service
public class WFichier implements Function<ResultSet, MFichier> {

    @Override
    public MFichier apply(ResultSet resultSet) {
        MFichier fichier = new MFichier();
        DateConverter ldt = new DateConverter(); // ldt --> localDateTime
        try {
            fichier.setIdFichier(resultSet.getInt("idFichier"));
            fichier.setIdEntite(resultSet.getInt("idEntite"));
            fichier.setNomFichier(resultSet.getString("nomFichier"));
            fichier.setTypeFichier(resultSet.getString("typeFichier"));
            fichier.setCreatedBy(resultSet.getString("createdBy"));
            fichier.setCreatedOn(ldt.dateTimeSQLtoLocalDateTime(resultSet, "createdOn"));
            fichier.setModifiedBy(resultSet.getString("modifiedBy"));
            fichier.setModifiedOn(ldt.dateTimeSQLtoLocalDateTime(resultSet, "modifiedOn"));
            fichier.setIsDeleted(resultSet.getBoolean("isDeleted"));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return fichier;
    }

}
