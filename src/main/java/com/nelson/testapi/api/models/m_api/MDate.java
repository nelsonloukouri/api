package com.nelson.testapi.api.models.m_api;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.sql.Date;
import java.sql.Time;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = false)
@Data
@SuperBuilder
@NoArgsConstructor
public class MDate extends MAuditAttribute {

    private int idDate;
    private Date date;
    private Time creerle;
    private LocalDateTime testldt;

}
































































