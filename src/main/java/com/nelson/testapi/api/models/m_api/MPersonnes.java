package com.nelson.testapi.api.models.m_api;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = false)
@Data
@SuperBuilder
@NoArgsConstructor
public class MPersonnes extends MAuditAttribute {

    private int idPersonne;
    private String nom;
    private String prenoms;
    private int age;
    private String email;
    private String motDePasse;
    private String ville;
    private String adresse;
    private String genre;

}
