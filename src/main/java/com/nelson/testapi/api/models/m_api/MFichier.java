package com.nelson.testapi.api.models.m_api;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = false)
@Data
@SuperBuilder
@NoArgsConstructor
public class MFichier extends MAuditAttribute {

    private int idFichier;
    private int idEntite;
    private String nomFichier;
    private String typeFichier;

}
