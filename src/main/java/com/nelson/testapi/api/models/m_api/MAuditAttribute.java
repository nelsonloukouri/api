package com.nelson.testapi.api.models.m_api;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@Data
@SuperBuilder
@NoArgsConstructor
abstract class MAuditAttribute {

    protected String createdBy;
    protected LocalDateTime createdOn;
    protected String modifiedBy;
    protected LocalDateTime modifiedOn;
    protected String isDeletedPar;
    protected LocalDateTime isDeletedOn;
    protected Boolean isDeleted;

}
