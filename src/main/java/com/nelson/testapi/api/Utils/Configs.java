package com.nelson.testapi.api.Utils;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class Configs {

    @Value("${mysql.host}")
    private String mysqlHost;

    @Value("${mysql.username}")
    private String mysqlUsername;

    @Value("${mysql.password}")
    private String mysqlPassword;

    @Value("${mysql.DRIVER}")
    private String mysqlDriver;

}
