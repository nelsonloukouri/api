package com.nelson.testapi.api.Utils;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalDateTime;

public class DateConverter {

    public LocalDateTime dateTimeSQLtoLocalDateTime(ResultSet resultSet, String columnDate) throws SQLException {

        if (columnDate.equals("createdOn") || columnDate.equals("modifiedOn") || columnDate.equals("isDeletedOn")) {
            Date dateSQL = resultSet.getDate(columnDate);
            Time timeSQL = resultSet.getTime(columnDate);
            if (dateSQL == null || timeSQL == null) {
                return null;
            }
            String datetime = dateSQL + "T" + timeSQL;
            return LocalDateTime.parse(datetime);
        }
        return null;
    }

}
