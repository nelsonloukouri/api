-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: api
-- ------------------------------------------------------
-- Server version	5.6.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `api_date`
--

DROP TABLE IF EXISTS `api_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_date` (
  `idDate` int(11) NOT NULL AUTO_INCREMENT,
  `date_` date DEFAULT NULL,
  `datetime_` datetime DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `modifiedBy` varchar(45) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `isDeletedBy` varchar(45) DEFAULT NULL,
  `isDeletedOn` datetime DEFAULT NULL,
  `isDeleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`idDate`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_date`
--

LOCK TABLES `api_date` WRITE;
/*!40000 ALTER TABLE `api_date` DISABLE KEYS */;
INSERT INTO `api_date` VALUES (1,'2024-04-12','2024-04-01 15:21:17',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(2,'2024-04-12',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(3,'2024-04-12','2024-04-01 15:21:42',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `api_date` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_fichier`
--

DROP TABLE IF EXISTS `api_fichier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_fichier` (
  `idFichier` int(11) NOT NULL AUTO_INCREMENT,
  `idEntite` int(11) DEFAULT NULL,
  `nomFichier` varchar(45) DEFAULT NULL,
  `typeFichier` varchar(45) DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `modifiedBy` varchar(45) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `isDeletedBy` varchar(45) DEFAULT NULL,
  `isDeletedOn` datetime DEFAULT NULL,
  `isDeleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`idFichier`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_fichier`
--

LOCK TABLES `api_fichier` WRITE;
/*!40000 ALTER TABLE `api_fichier` DISABLE KEYS */;
INSERT INTO `api_fichier` VALUES (1,1,'Extrait de naissance1.pdf','Extrait de naissance',NULL,NULL,NULL,NULL,NULL,NULL,_binary ''),(2,1,'2','3','Nelson Junior','2024-04-14 20:27:47',NULL,NULL,NULL,NULL,_binary '\0'),(3,1,'bnonjour.pdf','1','nelson','2024-04-21 23:40:04',NULL,NULL,NULL,NULL,_binary '\0');
/*!40000 ALTER TABLE `api_fichier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `api_personnes`
--

DROP TABLE IF EXISTS `api_personnes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `api_personnes` (
  `idPersonne` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) DEFAULT NULL,
  `prenoms` varchar(45) DEFAULT NULL,
  `age` int(3) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `createdBy` varchar(45) DEFAULT NULL,
  `createdOn` datetime DEFAULT NULL,
  `modifiedBy` varchar(45) DEFAULT NULL,
  `modifiedOn` datetime DEFAULT NULL,
  `isDeletedBy` varchar(45) DEFAULT NULL,
  `isDeletedOn` datetime DEFAULT NULL,
  `isDeleted` bit(1) DEFAULT NULL,
  PRIMARY KEY (`idPersonne`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `api_personnes`
--

LOCK TABLES `api_personnes` WRITE;
/*!40000 ALTER TABLE `api_personnes` DISABLE KEYS */;
INSERT INTO `api_personnes` VALUES (1,'LOUKOURI','Junior',21,'juniorloukouri@gmail.com',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `api_personnes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `clients`
--

DROP TABLE IF EXISTS `clients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `clients` (
  `CLIENT_ID` int(11) NOT NULL,
  `PRENOM` char(15) DEFAULT NULL,
  `NOM` char(20) NOT NULL,
  `RUE` char(25) DEFAULT NULL,
  `VILLE` char(20) DEFAULT NULL,
  `ETAT` char(2) DEFAULT NULL,
  `CODE_POSTAL` int(11) DEFAULT NULL,
  `TELEPHONE` char(13) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clients`
--

LOCK TABLES `clients` WRITE;
/*!40000 ALTER TABLE `clients` DISABLE KEYS */;
/*!40000 ALTER TABLE `clients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `nh_client`
--

DROP TABLE IF EXISTS `nh_client`;
/*!50001 DROP VIEW IF EXISTS `nh_client`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `nh_client` AS SELECT 
 1 AS `PRENOM`,
 1 AS `NOM`,
 1 AS `TELEPHONE`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `testtabletype`
--

DROP TABLE IF EXISTS `testtabletype`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `testtabletype` (
  `idnew_table` int(11) NOT NULL AUTO_INCREMENT,
  `testChar` char(5) DEFAULT NULL,
  PRIMARY KEY (`idnew_table`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `testtabletype`
--

LOCK TABLES `testtabletype` WRITE;
/*!40000 ALTER TABLE `testtabletype` DISABLE KEYS */;
INSERT INTO `testtabletype` VALUES (1,'b'),(2,'Bonjo'),(3,'j');
/*!40000 ALTER TABLE `testtabletype` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'api'
--

--
-- Dumping routines for database 'api'
--
/*!50003 DROP PROCEDURE IF EXISTS `ps_addDate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`nelson`@`%` PROCEDURE `ps_addDate`(
IN P_date date
)
BEGIN
	INSERT INTO api.api_date (
    date_,
    datetime_,
    isDeleted
    ) VALUES (
    P_date,
    now(),
    0
    );
    SELECT 
		idDate,
		date_,
        datetime_ AS creele,
		isDeleted
    FROM api.api_date
    WHERE idDate = last_insert_id();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ps_addfichier` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ps_addfichier`(
IN P_idEntite int(11),
IN P_nomFichier varchar(45),
IN P_typeFichier varchar(45),
IN P_createdBy varchar(45)
)
BEGIN
	INSERT INTO api.api_fichier (
    idEntite,
    nomFichier,
    typeFichier,
    createdBy,
    createdOn,
    isDeleted
    ) VALUES (
    P_idEntite,
    P_nomFichier,
    P_typeFichier,
    P_createdBy,
    now(),
    0
    );
    SELECT * FROM api.api_fichier
    WHERE idFichier = last_insert_id();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ps_addPersonnes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ps_addPersonnes`(
IN P_nom varchar(45),
IN P_prenoms varchar(45),
IN P_age int(3),
IN P_email varchar(45)
)
BEGIN
	INSERT INTO api.api_personnes (
    nom,
    prenoms,
    age,
    email,
    isDeleted
    ) VALUES (
    P_nom,
    P_prenoms,
    P_age,
    P_email,
    0
    );
    SELECT * FROM api.api_personnes
    WHERE idPersonne = last_insert_id();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ps_deleteFichier` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ps_deleteFichier`(
IN P_idFichier int(11)
)
BEGIN
	UPDATE api.api_fichier SET
	isDeleted = 1
    WHERE idFichier = P_idFichier AND isDeleted = 0;
    SELECT * FROM api.api_fichier
    WHERE idFichier = P_idFichier AND isDeleted = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ps_deletePersonnes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ps_deletePersonnes`(
IN P_idPersonne int(11)
)
BEGIN
	UPDATE api.api_personnes SET
    isDeleted = 1
    WHERE idPersonne = P_idPersonne AND isDeleted = 0;
    SELECT * FROM api.api_personnes
    WHERE idPersonne = P_idPersonne AND isDeleted = 1;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ps_getAllDate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`nelson`@`%` PROCEDURE `ps_getAllDate`()
BEGIN
	SELECT 
		idDate,
		date_,
        datetime_ AS creerle,
		isDeleted
    FROM api.api_date
    WHERE isDeleted = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ps_getAllFichier` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ps_getAllFichier`()
BEGIN
	SELECT * FROM api_fichier
    WHERE isDeleted = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ps_getAllPersonnes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ps_getAllPersonnes`()
BEGIN
	SELECT * FROM api.api_personnes
    WHERE isDeleted = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ps_updateFichier` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ps_updateFichier`(
IN P_idFichier int(11),
IN P_idEntite int(11),
IN P_nomFichier varchar(45),
IN P_typeFichier varchar(45)
)
BEGIN
	UPDATE api.api_fichier SET
	idEntite = P_idEntite,
	nomFichier = P_nomFichier,
	typeFichier = P_typeFichier
    WHERE idFichier = P_idFichier AND isDeleted = 0;
    SELECT * FROM api.api_fichier
    WHERE idFichier = P_idFichier AND isDeleted = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ps_updatePersonnes` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `ps_updatePersonnes`(
IN P_idPersonne int(11),
IN P_nom varchar(45),
IN P_prenoms varchar(45),
IN P_age int(3),
IN P_email varchar(45)
)
BEGIN
	UPDATE api.api_personnes SET
    idPersonne = P_idPersonne,
	nom = P_nom,
	prenoms = P_prenoms,
	age = P_age,
	email = P_email
    WHERE idPersonne = P_idPersonne AND isDeleted = 0;
    SELECT * FROM api.api_personnes
    WHERE idPersonne = P_idPersonne AND isDeleted = 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `nh_client`
--

/*!50001 DROP VIEW IF EXISTS `nh_client`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `nh_client` AS select `clients`.`PRENOM` AS `PRENOM`,`clients`.`NOM` AS `NOM`,`clients`.`TELEPHONE` AS `TELEPHONE` from `clients` where (`clients`.`ETAT` = 'NH') */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-04-24  8:24:04
