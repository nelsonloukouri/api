package com.nelson.testapi.api;

import com.nelson.testapi.api.TestTechno.AspectJ.Account;
import com.nelson.testapi.api.TestTechno.AspectJ.AnnotatingAspectJ.SecuredMethod;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AccountUnitTest {

    private Account account;

    @Before
    public void before() {
        account = new Account();
    }

    @Test
    public void givenBalance20AndMinBalance10_whenWithDraw5_thenSuccess() {
        assertTrue(account.withdraw(5));
    }

    @Test
    public void givenBalance20AndMinBalance10_whenWithdraw100_thenFail() {
        assertFalse(account.withdraw(100));
    }

    @Test
    public void testMethod() throws Exception {
        SecuredMethod service = new SecuredMethod();
        service.unLockedMethode();
        service.lockedMethod();
    }

}
